#pragma once
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h>
#include <stdio.h>
#include "Crc32.h"
#include "Coronet.h"
#include "Constants.h"
#include <errno.h>


class CoronaAdapter : public Coronet::ITransmitter, public Coronet::IReceiver
{
public:
    CoronaAdapter();
    ~CoronaAdapter();

    /*
    * @brief Send some data to your target, with slightly virus effects.
    * 
    * @param buffer        The buffer to send.
    * @param bufferSize    The buffer size. (in bytes)
    * 
    * @return ssize_t      The bytes amount that have been sent.
    *                      If it's an error return -ERRNO:
    *                      -EINVAL     In case of invalid length (bigger than MAX_DATA_SIZE)
    */
    ssize_t Send(const uint8_t* data, size_t dataSize);
    
    /*
    * @brief Receive some data from your target, with slightly virus effects.
    * 
    * @param o_buffer      The buffer to receive into the data.
    * @param bufferSize    The buffer size. (in bytes)
    * 
    * @return ssize_t      The bytes amount that have been received.
    *                      If it's an error return -ERRNO.
    */
    ssize_t Receive(uint8_t* o_buffer, size_t bufferSize);

    /*
    *   Initialization Method For Corona Adapter Sender Side-
    *   Creates a Socket For Sending Data To A Certain Address
    * 
    *   @param address      Address For Sending
    * 
    *   @return bool        True If Successful, False Otherwise
    */
    bool Init(sockaddr_in& address);

    /*
    *   Initialization Method For Corona Adapter Receiver Side-
    *   Creates a Socket For Listening To A Certain Port
    * 
    *   @param port     Port For Listening
    * 
    *   @return bool    True If Successful, False Otherwise
    */
    bool Init(uint16_t port);

    
private:

    /*
    *   Sets Receive Timeout On The Socket
    *   
    *   @param microSec     Microseconds To Wait For A Message Before
    *                       Timeout
    *   
    *   @return bool    True If Successful, False Otherwise
    */
    bool SetTimeout(suseconds_t microSec);

private:

    int32_t m_socketFD;
    sockaddr_in m_connectionAddress;
};