#pragma once
#include <stdio.h>
#include <list>
#include <bitset>
#include "CoronaAdapter.h"
#include "Messages.h"

enum FileStatus
{
    FinishedReading,
    LeftToRead
};

class FileSender
{
public:
    FileSender();

    ~FileSender();

    /*
    *   Initializes A File Sender
    * 
    *   @param address      Receiver's Address Struct
    *   @param harmness     Network State
    *   @param filePath     Path Of File To Send
    * 
    *   @return int32_t     On Success Pipe FD That The Progress
    *                       Will Be Sent To
    *                       ERROR(-1) On Failure 
    */
    int32_t Init(sockaddr_in& address, Coronet::Harmness harmness, char* filePath);

    /*
    *   Main Loop For Sending The File
    */
    void Run();

private:

    /*
    *   Gets The File Name From A File Path
    *   
    *   @param filePath     File Path To Get It's Name
    * 
    *   (In This Case Puts The Output In A Member Of The Class
    *    But Easily An Output Param Can Be Added To Make The Method Generic)
    */
    void GetFileName(char* filePath);

    /*
    *   Send The Start Connection Info(File Name And Size)
    *   And Don't Start Sending The File Until Sure The Receiver
    *   Got The Start Packet
    */
    const void SendStart();

    /*
    *   Reads BUFFER_SIZE Bytes From The File And Creates A Packet From Them
    * 
    *   @return FileStatus  FinishedReading If Finished Reading
    *                       LeftToRead If There Is More Data To Read 
    */
    FileStatus CreatePacket();

    /*
    *   Sends The First WINDOW_SIZE Or m_ReadyMessages.size() (The Smaller Of Them)
    *   Packets From m_readyMessages To The Receiver
    */
    const void SendPackets();

    /*
    *   Removes A Packet From m_readyMessages
    * 
    *   @param ack  Seq Number Of Packet To Remove
    */
    void RemoveReceived(uint32_t ack);

    /*
    *   Listen To Ack Messages From The Receiver
    * 
    *   @return uint16_t    Number Of Ack Messages Received
    */
    const uint16_t CheckResponse();

private:

    Coronet::Coronet* m_coronet;
    CoronaAdapter m_adapter;
    FILE* m_file;
    std::list<Message> m_readyMessages;
    std::bitset<MAX_FILE_PACKETS> m_sucRecvMsgs;
    int32_t m_managerPipe;
    uint32_t m_lastReadChunk;
    sockaddr_in m_dest;
    uint32_t m_fileChunks;
    char m_fileName[FILENAME_LENGTH];
    bool m_finishedReading;
    bool m_finishedSending;
};