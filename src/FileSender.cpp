#include "FileSender.h"

FileSender::FileSender() : m_lastReadChunk(0), m_file(nullptr), m_finishedSending(false),
    m_coronet(nullptr), m_managerPipe(ERROR)
{
}

FileSender::~FileSender()
{
    if(m_file != nullptr)
    {
        fclose(m_file);
    }
    if(m_coronet != nullptr)
    {
        delete m_coronet;
    }
    if(m_managerPipe != ERROR)
    {
        close(m_managerPipe);
    }
}

int32_t FileSender::Init(sockaddr_in& address, Coronet::Harmness harmness, char* filePath)
{

    memcpy(&m_dest, &address, sizeof(sockaddr_in));

    //Init Adapter And Check If Failed
    if(!m_adapter.Init(address))
    {
        printf("Failed Initializing Network Adapter");
        return ERROR;
    }

    //Open File And Read It
    m_file = fopen(filePath, "r");
    if(m_file == nullptr)
    {
        printf("Error Opening File\n");
        return ERROR;
    }

    //Get The File Name From Absulute Path
    GetFileName(filePath);

    //Get File Size To Calculate Precentages Sucessfully Sent
    fseek(m_file, 0L, SEEK_END);
    int64_t fileSize= ftell(m_file);

    //Return Cursor To The Beggining
    rewind(m_file);

    m_fileChunks = fileSize/BUFFER_SIZE + 1;

    //Create Coronet Instance And Check If Successfull
    m_coronet = new Coronet::Coronet(m_adapter, m_adapter, harmness);
    if(m_coronet == nullptr)
    {
        printf("Error Creating Network Library\n");
        return ERROR;
    }

    //Create Pipe
    int32_t pipeFDS[2];
    if(pipe(pipeFDS) == ERROR)
    {
        printf("Failed Creating Pipe\n");
        return ERROR;
    }
    //Save Writing End
    m_managerPipe = pipeFDS[1];

    //Return Reading End
    return pipeFDS[0];
}


void FileSender::GetFileName(char* filePath)
{
    //Copy The Path To A Separate Array Because strtok Changes The Str
    char temp[FILENAME_LENGTH] = {};
    memcpy(temp, filePath, FILENAME_LENGTH);
    //Get The Next Element And Check If It's The Last
    char* name;
    char* tempName = strtok(temp, "/");
    while(tempName != nullptr)
    {
        name = tempName;
        tempName = strtok(NULL, "/");
    }
    //When It's The Last, Copy To The Memeber Array
    strncpy(m_fileName, name, FILENAME_LENGTH);
}


const void FileSender::SendStart()
{
    //Create start packet
    Message start;
    start.start.numOfPackets = m_fileChunks;
    start.start.header.type = Start;
    memcpy(start.start.fileName, m_fileName, FILENAME_LENGTH);
    start.start.header.checkSum = 0;
    start.start.header.checkSum = crc32_16bytes(&start.msg, sizeof(StartMsg));
    Message startToSend;
    memcpy(&startToSend, &start, sizeof(StartMsg));
    //Send start packet
    m_coronet->Send(startToSend.msg, sizeof(StartMsg));
    //Read for ack
    bool startCon = false;
    Message received;
    ssize_t bytesRead = m_coronet->Receive(received.msg, sizeof(AckMsg));
    while(!startCon)
    {
        //printf("ChkSum %u\n", start.start.header.checkSum);
        //If message is smaller than expected then send and read again
        if(bytesRead == SOCK_TIMEOUT)
        {
            memcpy(&startToSend, &start, sizeof(StartMsg));
            m_coronet->Send(startToSend.msg, sizeof(StartMsg));
            bytesRead = m_coronet->Receive(received.msg, sizeof(AckMsg));
        }
        else
        {
            //Check chksum and opcode
            uint32_t chkSum = received.start.header.checkSum;
            received.ack.header.checkSum = 0;
            if(chkSum == crc32_16bytes(&received.msg, sizeof(AckMsg)) 
                    && received.ack.opcode == OkStart)
            {
                //printf("Received Start Ack\n");
                startCon = true;
            }
            //If wrong send again
            else
            {
                memcpy(&startToSend, &start, sizeof(StartMsg));
                m_coronet->Send(startToSend.msg, sizeof(StartMsg));
                bytesRead = m_coronet->Receive(received.msg, sizeof(AckMsg));
            }
        }
        
    }
}


FileStatus FileSender::CreatePacket()
{
    //To Hold The New Message
    Message temp;
    //Read From File

    size_t bytesRead = fread(&temp.data.data, 1, BUFFER_SIZE, m_file);

    //If Fineshed Reading
    if(bytesRead == 0)
    {
        return FinishedReading;
    }
    //Organize MetaData
    temp.data.len = bytesRead;
    temp.data.seq = m_lastReadChunk;
    temp.data.header.type = Data;
    temp.data.header.checkSum = 0;
    temp.data.header.checkSum = crc32_16bytes(&temp.msg, sizeof(Message));

    //Add To Ready Messages
    m_readyMessages.push_back(temp);
    m_lastReadChunk++;
    return LeftToRead;
}


const void FileSender::SendPackets()
{
    //Get the size of the packets list
    size_t packetsListSize = m_readyMessages.size();
    Message toSend;
    std::list<Message>::iterator it = m_readyMessages.begin();
    ssize_t bytesSent;
    //Send WINDOW_SIZE packets, or less if the ready packets are fewer
    for(size_t i = 0; i < WINDOW_SIZE && i < packetsListSize; i++)
    {
        toSend = *it;
        bytesSent = m_coronet->Send(toSend.msg, sizeof(Message));
        //If there was a problem with the send, send again
        while (bytesSent < sizeof(Message))
        {
            bytesSent = m_coronet->Send(toSend.msg, sizeof(Message));
        }
        it++;
    }
}


void FileSender::RemoveReceived(uint32_t ack)
{
    std::list<Message>::iterator it = m_readyMessages.begin();
    size_t listSize = m_readyMessages.size();
    //Loop trough list
    for(size_t i = 0; i < listSize; i++)
    {
        //Check if found the right packet
        if((*it).data.seq == ack)
        {
            //remove it from the list
            it = m_readyMessages.erase(it);
            m_sucRecvMsgs[ack] = true;
            return;
        }
        it++;
    }
}


const uint16_t FileSender::CheckResponse()
{
    uint16_t recvCount = 0;
    Message received;
    int32_t chkSum;
    //Get Message From Network
    ssize_t bytesReceived = m_coronet->Receive(received.msg, sizeof(AckMsg));
    //If There Is A Message
    while (bytesReceived > 0)
    {
        //Check Chksum
        chkSum = received.ack.header.checkSum;
        received.ack.header.checkSum = 0;
        if((chkSum == crc32_16bytes(&received.msg, sizeof(AckMsg))))
        {
            if(received.ack.opcode == OkEnd)
            {
                m_finishedSending = true;
                return recvCount;
            }
            //Check If Ack Already Received Before
            else if(received.ack.opcode == Ok && !m_sucRecvMsgs.test(received.ack.ack) )
            {
                //Remove From Pending List
                RemoveReceived(received.ack.ack);
                //Mark As Received
                m_sucRecvMsgs.set(received.ack.ack);
                //Inc Count
                recvCount++;
            }
        }
        //Read Next Message
        bytesReceived = m_coronet->Receive(received.msg, sizeof(AckMsg));
    }
    return recvCount;
}


void FileSender::Run()
{

    //Start Packet
    SendStart();
    uint32_t sucSent = 0;
    uint8_t recvCount = 0;
    while(!m_finishedSending)
    {
        //Create New Packets
        if(!m_finishedReading)
        {
            for(size_t i = 0; i < WINDOW_SIZE - recvCount; i++)
            {
                if(CreatePacket() == FinishedReading)
                {
                    m_finishedReading = true;
                    break;
                }
            }
        }
        SendPackets();
        recvCount = CheckResponse();
        sucSent += recvCount;
        //Write To Manager Progress
        write(m_managerPipe, &sucSent, sizeof(uint32_t));
    }
    //Validate That Manager Knows That Sender Finished
    write(m_managerPipe, &m_fileChunks, sizeof(uint32_t));
}